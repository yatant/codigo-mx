var Cookies = require('cookies');
let keys = ['keyboard cat'];

module.exports.allPosts = (req, res) => {
  Post.findAll({
    order: [
      ['post_id', 'DESC']
    ],
  }).then(function (posts) {
    res.status(200).json(posts);
  }, function (err) {
    res.status(200).json({err});
  });
};

module.exports.allResponses = (req, res) => {
  Response.findAll({where: {post_id: req.params.id}}).then(posts => {
    res.status(200).json(posts);
  }, function (err) {
    res.status(200).json({err});
  });
};


module.exports.post = (req, res) => {
  let cookies = new Cookies(req, res, {keys: keys});
  let user = JSON.parse(cookies.get('logged', {signed: true}));
  Post.findOne({where: {post_id: req.params.id}}).then(post => {
    res.status(200).render("pages/post", {post, user});
  }, err => {
    res.status(200).json({err});
  });
};

module.exports.createPost = (req, res) => {
  let cookies = new Cookies(req, res, {keys: keys});
  let user = JSON.parse(cookies.get('logged', {signed: true}));
  let body = req.body;
  const post = Post.build({
    user_name: user.username,
    post_content: body.post_content,
    responses_count: 0
  });

  post.save().then(() => {
    res.status(200).json(post);
  });
};

module.exports.addResponse = (req, res) => {
  let cookies = new Cookies(req, res, {keys: keys});
  let user = JSON.parse(cookies.get('logged', {signed: true}));
  let body = req.body;
  const response = Response.build({
    user_name: user.username,
    response_content: body.response_content,
    post_id: body.post_id
  });

  response.save().then(() => {

    Post.findOne({where: {post_id: req.body.post_id}}).then(post => {
      post.responses_count =  post.responses_count + 1;
      post.save();
      res.status(200).json(response);
    }, err => {
      res.status(200).json(response);
    });

  });
};

module.exports.deletePost = (req, res) => {
  let cookies = new Cookies(req, res, {keys: keys});
  let user = JSON.parse(cookies.get('logged', {signed: true}));
  Post.findOne({where: {post_id: req.body.post_id}}).then(post => {
    if (post.user_name === user.username) {
      Response.destroy({where: {post_id: req.body.post_id}}).then(() => {
        post.destroy().then(u => {
          res.status(200).json({u});
        });
      });
    } else {
      res.status(500).json({response: "no tienes permisos para eliminar"});
    }

  }, err => {
    res.status(200).json({err});
  });

};
