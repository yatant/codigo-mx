var Cookies = require('cookies');
let md5 = require('md5');
let keys = ['keyboard cat'];

module.exports.profile = (req, res) => {
  let cookies = new Cookies(req, res, {keys: keys});
  let user = cookies.get('logged', {signed: true});
  if(typeof user === "undefined"){
    return res.status(500).json({err, ookiesd});
  }

  User.findOne({where: {username: JSON.parse(user).username}}).then(user => {
    res.status(200).json(user);
  }, function (err) {
    res.status(200).json({err, ookiesd});
  });
};

module.exports.register = (req, res) => {
  let cookies = new Cookies(req, res, {keys: keys});
  let body = req.body;
  const user = User.build({
    username: body.username,
    password: md5(body.password),
    name: body.name,
    lastname: body.lastname
  });

  user.save().then(() => {
    cookies.set('logged', JSON.stringify(user), {signed: true});
    res.status(200).json(user);
  });
};

module.exports.login = (req, res) => {
  User.findOne({where: {username: req.body.username, password: md5(req.body.password)}}).then(user => {
    let cookies = new Cookies(req, res, {keys: keys});
    cookies.set('logged', JSON.stringify(user), {signed: true});
    res.status(200).json(user);
  }, function (err) {
    res.status(200).json({err});
  });
};


module.exports.logout = (req, res) => {
  res.clearCookie("logged");
  res.redirect("/");
};
