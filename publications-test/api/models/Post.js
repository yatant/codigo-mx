module.exports = {
  attributes: {
    post_id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER
    },
    user_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    responses_count: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    post_content: {
      type: Sequelize.STRING,
      allowNull: false
    }
  },
  options: {
    tableName: 'Post',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    scopes: {}
  }
};
