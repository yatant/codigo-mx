module.exports = {
  attributes: {
    response_id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER
    },
    post_id: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    user_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    response_content: {
      type: Sequelize.STRING,
      allowNull: false
    }
  },
  options: {
    tableName: 'Response',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
    scopes: {}
  }
};
