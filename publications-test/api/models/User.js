module.exports = {
  attributes: {
    idUser: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lastname: {
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false
    }
  },
  options: {
    tableName: 'User',
    classMethods: {},
    instanceMethods: {},
    hooks: {
      beforeSave: (instance, options) => {
        instance.name = instance.name.charAt(0).toUpperCase() + instance.name.slice(1);
        instance.lastname = instance.lastname.charAt(0).toUpperCase() + instance.lastname.slice(1);
      }
    },
    scopes: {}
  }
};
