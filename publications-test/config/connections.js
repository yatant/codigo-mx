module.exports.connections = {
  mysql: {
    port: 3306,
    user: 'root',
    password: '',
    database: 'publications',
    options: {
      host: 'localhost',
      port: 3306,
      dialect: 'mysql',
      logging: console.log
    }
  }
};
