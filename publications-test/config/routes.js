/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  '/': {view: 'pages/sign'},

  //post
  'get /posts': {view: 'pages/posts'},
  'get /allPosts': 'PostController.allPosts',
  'post /createPost': 'PostController.createPost',
  'post /addResponse': 'PostController.addResponse',
  'get /allResponses/:id': 'PostController.allResponses',
  'get /post/:id': 'PostController.post',
  'delete /deletePost': 'PostController.deletePost',

  //user
  'get /profile': 'UserController.profile',


  //login
  'post /user/login': 'UserController.login',
  'get /logout': 'UserController.logout',
  'post /user/register': 'UserController.register'
};
